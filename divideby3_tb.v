`timescale 1ns/1ns

module divideby3_tb;

reg clk;
reg reset;
wire q;

divideby3 DUT (.clk(clk), .reset(reset), .q(q));

always

#5 clk=~clk;


 initial 
    begin
      clk=0;
      reset=0;
      $dumpfile("divideby3.vcd");
      $dumpvars(0,divideby3_tb);
      $monitor ("\n%t: CLK=%b - Q=%b (RST=%b)\n",
                $time,clk, q, reset);
    end

  initial // Testing the device
    begin
      #50
      reset=1;
      #50 //
      reset=0;
      #150
      $finish;
    end

endmodule